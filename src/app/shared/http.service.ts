import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../user/user.model';
@Injectable({
    providedIn: 'root',
})
export class HttpService {
constructor(private httpClient: HttpClient) { }
baseUrl =
'https://656de509bcc5618d3c242ce0.mockapi.io/test/User'
;
    public getUsers(): Observable<User[]> {
        return this.httpClient.get<User[]>(this.baseUrl); 
    }

    public getUser(id: string): Observable<User> {
        return this.httpClient.get<User>(this.baseUrl + '/' + id);
    }

    public deleteUser(id: string): Observable<User> {
        return this.httpClient.delete<User>(this.baseUrl + '/' + id);
    }

    public addUser(user: User): Observable<User> {
        return this.httpClient.post<User>(this.baseUrl, user);
    }

    public updateUser(user: User): Observable<User> {
        return this.httpClient.put<User>(this.baseUrl + '/' + user.id, user);
    }
}