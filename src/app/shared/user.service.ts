import { Injectable } from '@angular/core';
import { User } from '../user/user.model';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { HttpService } from './http.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {


  users = new BehaviorSubject<User[]>([]);

  constructor(private router: Router, private http: HttpService) { }

    getUsers(): void {
        this.http
            .getUsers()
            .subscribe((user) => this.users.next(user));
    }

    getUser(id: string): Observable<User> {
        return this.http.getUser(id);
    }

    deleteUser(id: string){
        this.http
            .deleteUser(id)
            .subscribe((user) => {
                const updatedUsers = this.users.value.filter(user => user.id !== id);
                this.users.next(updatedUsers);
                console.log('User deleted');
            }
        )
    }

    addUser(user: User){
        this.http
            .addUser(user)
            .subscribe((user) => {
                const updatedUsers = [...this.users.value, user];
                this.users.next(updatedUsers);
                console.log('User added');
            }
        )
    }

    updateUser(user: User){
        this.http
            .updateUser(user)
            .subscribe((user) => {
                const updatedUsers = this.users.value.map((u) => {
                    if (u.id === user.id) {
                        return user;
                    }
                    return u;
                });
                this.users.next(updatedUsers);
                console.log('User updated');
            }
        )
    }
}