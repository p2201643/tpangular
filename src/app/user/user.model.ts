export class User {
    id!: string ;
    name!: string ;
    occupation!: string ;
    email!: string ;
    bio!: string ;
}