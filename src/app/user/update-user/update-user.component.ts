import { Component } from '@angular/core';
import { UserService } from '../../shared/user.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup} from '@angular/forms';
import { User } from '../user.model';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent {
    myForm: FormGroup;
    user !: User;
    id !: string;

    constructor(fb: FormBuilder, private userService: UserService, private router: Router, private route: ActivatedRoute) {        
        this.myForm = fb.group({
            'name': [''],
            'email': [''],
            'occupation': [''],
            'bio': ['']
        });
    }

    ngOnInit(): void {
        let id : string = this.route.snapshot.paramMap.get('id') ? this.route.snapshot.paramMap.get('id')! : '';
        console.log(id);
        this.userService.getUser(id).subscribe((user) => {
            this.user = user;
            this.myForm.setValue({
                name: this.user.name,
                email: this.user.email,
                occupation: this.user.occupation,
                bio: this.user.bio
            })
            this.id = this.user.id;
        });
    }

    onSubmit(): void {
        let user : User = new User();
        user.id = this.user.id;
        user.name = this.myForm.value.name;
        user.email = this.myForm.value.email;
        user.occupation = this.myForm.value.occupation;
        user.bio = this.myForm.value.bio;
        this.userService.updateUser(user);
        console.log(user);
        this.router.navigate(['/users']);
    }
}
