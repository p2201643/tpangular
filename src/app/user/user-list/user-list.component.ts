import { Component } from '@angular/core';
import { User } from '../user.model';
import { UserService } from '../../shared/user.service';
import { Subscription } from 'rxjs';
import {MatTableDataSource} from '@angular/material/table';
import { ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})

export class UserListComponent {
    constructor(private userService: UserService){}

    title = 'User List'
    userList!: User[]
    displayedUserList = new MatTableDataSource<User>();
    subscriptions: Subscription[] = [];
    displayedColumns: string[] = ['name', 'email', 'occupation', 'details', 'update', 'delete'];

    nameInput = new FormControl('');
    emailInput = new FormControl('');
    occupationInput = new FormControl('');

    @ViewChild(MatPaginator) paginator !: MatPaginator;

    ngOnInit(): void {
        this.userService.getUsers();
        let subscription = this.userService.users.subscribe(
            (users) => (
                this.userList = users,
                this.displayedUserList = new MatTableDataSource<User>(this.userList),
                this.displayedUserList.paginator = this.paginator
            )
        );
        this.subscriptions.push(subscription);
    }

    deleteUser(id: string){
        this.userService.deleteUser(id);
    }

    filterByInputs() {
        const name = this.nameInput.value ? this.nameInput.value.trim().toLowerCase() : '';
        const email = this.emailInput.value ? this.emailInput.value.trim().toLowerCase() : '';
        const occupation = this.occupationInput.value ? this.occupationInput.value.trim().toLowerCase() : '';
    
        this.displayedUserList.filterPredicate = (data: User, filter: string) => {
            return data.name.toLowerCase().startsWith(name) &&
                   data.email.toLowerCase().startsWith(email) &&
                   data.occupation.toLowerCase().startsWith(occupation);
        };

        this.displayedUserList.filter = name + email + occupation;
    }
}
