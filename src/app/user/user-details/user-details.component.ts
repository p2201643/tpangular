import { Component } from '@angular/core';
import { User } from '../user.model';
import { UserService } from '../../shared/user.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent {
    constructor(private route: ActivatedRoute, private userService: UserService){}

    title = 'User Details'
    user!: User

    ngOnInit(): void {
        let id : string = this.route.snapshot.paramMap.get('id') ? this.route.snapshot.paramMap.get('id')! : '';
        console.log(id);
        this.userService.getUser(id).subscribe((user) => {
            this.user = user;
        });
    }
}
