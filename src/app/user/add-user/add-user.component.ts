import { Component } from '@angular/core';
import { UserService } from '../../shared/user.service';
import { Router } from '@angular/router';

import { FormBuilder, FormGroup} from '@angular/forms';
import { User } from '../user.model';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent {
    myForm: FormGroup;

    constructor(fb: FormBuilder, private userService: UserService, private router: Router) {
        this.myForm = fb.group({
            'name': [''],
            'email': [''],
            'occupation': [''],
            'bio': ['']
        });
    }

    onSubmit(): void {
        let user : User = new User();
        user.name = this.myForm.value.name;
        user.email = this.myForm.value.email;
        user.occupation = this.myForm.value.occupation;
        user.bio = this.myForm.value.bio;

        console.log(user);
        this.userService.addUser(user);
        this.router.navigate(['/users']);
    }
}
